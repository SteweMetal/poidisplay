# PoiDisplay

Test exercise implementation for Mondriaan DC by Juhos Istv�n

The final state of the code base can be found on the [`develop`](https://bitbucket.org/SteweMetal/poidisplay/src/develop/) branch.

A bug in Bitbucket could prevent the rendering of the images in this README. In that case, please have a look at them in the [*images*](images) folder.

*Note: As an exercise and a challenge for myself, I have implemented the application in Kotlin using part of the libraries and technologies I am familiar with. However, the commits are less orderly than the main implementations' due to the strict time schedule I had while working on this version. The Kotlin version can be found on the [feature/kotlin-implementation](https://bitbucket.org/SteweMetal/poidisplay/src/79c31d9f8c6470b7bfa46b944b44e709ef2a4e8e/?at=feature%2Fkotlin-implementation) branch.*

![PoiDisplay_screens](images\PoiDisplay_screens.png)

## Development environment

I used the following tools and technologies to develop the project:

  * Java
  * Android Studio 3.2.1
  * Android Gradle plugin 3.2.1
  * Gradle
  * Git
    * I followed the GitFlow workflow as far as it was reasonable for a project of this size
  * A minimal set of `androidx` libraries
    * `AppCompat`, version 1.0.2
    * `RecyclerView`, version 1.0.0
  * A minimal set of libraries from Google
    * Android Material library, version 1.0.0
    * Google Maps, version 16.0.0

The project uses Java 8 language features.

## Architecture

The project was built upon a variant of the MVP architecture. The application utilizes a single-`Activity` approach while multiple `Fragment` classes are acting as screens.

Below are the schematic diagrams of the application's architecture, data flow, model conversion border and  threading:

![PoiDisplayArchitecture](images\PoiDisplayArchitecture.png)

*Note: While the V in MVP refers to the View components, calling the actual classes and interfaces Views would logically conflict with `Views` in Android terminology. Therefore, I call such classes `Screens`.*

