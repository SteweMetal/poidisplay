package hu.stewemetal.demo.poidisplay.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * The {@link Poi} class is the main domain model of the application. Instances of this class are immutable objects.
 * <p>
 * Note: In Kotlin, this would be a data class with immutable public properties.
 */
public class Poi implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Poi createFromParcel(Parcel parcel) {
            return new Poi(parcel);
        }

        public Poi[] newArray(int size) {
            return new Poi[size];
        }
    };

    private String name;

    private float lat;

    private float lon;

    public Poi(String name, float lat, float lon) {
        this.name = name;
        this.lat = lat;
        this.lon = lon;
    }

    public Poi(Parcel parcel) {
        this.name = parcel.readString();
        this.lat = parcel.readFloat();
        this.lon = parcel.readFloat();
    }

    public String getName() {
        return name;
    }

    public float getLat() {
        return lat;
    }

    public float getLon() {
        return lon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeFloat(lat);
        dest.writeFloat(lon);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Poi poi = (Poi) o;

        if (Float.compare(poi.lat, lat) != 0) return false;
        if (Float.compare(poi.lon, lon) != 0) return false;
        return name != null ? name.equals(poi.name) : poi.name == null;
    }
}
