package hu.stewemetal.demo.poidisplay.network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import hu.stewemetal.demo.poidisplay.domain.model.Poi;
import hu.stewemetal.demo.poidisplay.network.exception.NetworkCallException;
import hu.stewemetal.demo.poidisplay.network.model.NetworkPoi;

/**
 * The {@link NetworkDataSource} class handles network communication. It is responsible for
 * acquiring data from the network in the network model format and mapping it to domain model format.
 * <p>
 * In the scope of this application, the mentioned mapping only happens exactly one way:
 * from {@link NetworkPoi} to {@link Poi} objects.
 */
public class NetworkDataSource {

    private PoiDisplayApi poiDisplayApi;

    public NetworkDataSource(@NonNull PoiDisplayApi poiDisplayApi) {
        this.poiDisplayApi = poiDisplayApi;
    }

    public List<Poi> getPois() throws IOException, NetworkCallException {
        List<NetworkPoi> networkResult = poiDisplayApi.getPois();

        List<Poi> result = new ArrayList<>(networkResult.size());

        for (NetworkPoi networkPoi : networkResult) {
            result.add(
                    new Poi(
                            networkPoi.getName(),
                            Float.valueOf(networkPoi.getLat()),
                            Float.valueOf(networkPoi.getLon())
                    )
            );
        }

        return result;
    }

}
