package hu.stewemetal.demo.poidisplay.network;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import hu.stewemetal.demo.poidisplay.network.exception.NetworkCallException;
import hu.stewemetal.demo.poidisplay.network.model.NetworkPoi;

import static hu.stewemetal.demo.poidisplay.network.NetworkConfig.SERVICE_BASE_URL;

/**
 * A verbose implementation of a network API call and a JSON-to-object mapping.
 * <p>
 * A modern way to implement the same functionality would be to use Retrofit with OkHttp for API
 * call generation and Gson or some other serialization library for the JSON mapping.
 */
public class PoiDisplayApi {

    List<NetworkPoi> getPois() throws IOException, NetworkCallException {
        String resultString = getStringFromUrl(SERVICE_BASE_URL + "/pois.json");

        return convertStringToPois(resultString);
    }

    private List<NetworkPoi> convertStringToPois(String resultString) throws IOException {
        try {
            JSONObject jsonObject = new JSONObject(resultString);

            JSONArray pois = jsonObject.getJSONArray("pois");

            List<NetworkPoi> result = new ArrayList<>(pois.length());

            for (int i = 0; i < pois.length(); i++) {
                JSONObject poiData = pois.getJSONObject(i);

                result.add(
                        new NetworkPoi(
                                poiData.getString("name"),
                                poiData.getString("lat"),
                                poiData.getString("long")
                        )
                );
            }

            return result;

        } catch (JSONException e) {
            throw new IOException(e);
        }
    }

    private String getStringFromUrl(String urlString) throws IOException, NetworkCallException {
        StringBuilder result = new StringBuilder();

        HttpURLConnection urlConnection = null;

        try {
            URL url = new URL(urlString);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            int responseCode = urlConnection.getResponseCode();
            if (responseCode < 200 || responseCode >= 300) {
                throw new NetworkCallException(responseCode);
            }

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

            return result.toString();

        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }
}
