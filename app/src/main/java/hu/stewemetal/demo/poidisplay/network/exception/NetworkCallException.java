package hu.stewemetal.demo.poidisplay.network.exception;

public class NetworkCallException extends Exception {

    public NetworkCallException(int responseCode) {
        super("Network response code was " + responseCode);
    }
}
