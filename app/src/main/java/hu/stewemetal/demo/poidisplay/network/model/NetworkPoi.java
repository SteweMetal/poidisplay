package hu.stewemetal.demo.poidisplay.network.model;

/**
 * The {@link NetworkPoi} class represents a POI descriptor
 * received from a network call. Instances of this class are immutable objects.
 *
 * Note: In Kotlin, this would be a data class with immutable public properties.
 */
public class NetworkPoi {

    private String name;

    private String lat;

    private String lon;

    public NetworkPoi(String name, String lat, String lon) {
        this.name = name;
        this.lat = lat;
        this.lon = lon;
    }

    public String getName() {
        return name;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    @Override
    public String toString() {
        return "NetworkPoi{" +
                "name='" + name + '\'' +
                ", lat='" + lat + '\'' +
                ", lon='" + lon + '\'' +
                '}';
    }
}
