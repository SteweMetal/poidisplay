package hu.stewemetal.demo.poidisplay.ui;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import hu.stewemetal.demo.poidisplay.ui.navigation.Navigator;


public class NavFragment extends Fragment {

    @NonNull
    protected Navigator navigator;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity = requireActivity();

        if (activity instanceof Navigator) {
            navigator = (Navigator) activity;
        } else {
            throw new IllegalStateException("Fragments of this application are only allowed to " +
                    "be attached to an Activity that implements the Navigator interface!");
        }
    }
}
