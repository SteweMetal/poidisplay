package hu.stewemetal.demo.poidisplay.ui;

public class Presenter<T extends Screen> {
    protected T screen;

    public void attachScreen(T screen) {
        this.screen = screen;
    }

    public void detachScreen() {
        screen = null;
    }
}
