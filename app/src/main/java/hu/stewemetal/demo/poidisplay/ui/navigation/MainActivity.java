package hu.stewemetal.demo.poidisplay.ui.navigation;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import hu.stewemetal.demo.poidisplay.R;
import hu.stewemetal.demo.poidisplay.ui.poilist.PoiListFragment;

/**
 * The single Activity of the application with the one responsibility of handling
 * Fragment transactions.
 */
public class MainActivity extends AppCompatActivity implements Navigator {

    private FragmentManager fragmentManager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            add(new PoiListFragment());
        }
    }

    @Override
    public void add(Fragment fragment) {
        if (fragmentManager.isStateSaved()) {
            return;
        }

        String fragmentNavTag = fragment.getClass().getName();

        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.fragment_transition_in, R.anim.fragment_transition_out,
                        R.anim.fragment_transition_in, R.anim.fragment_transition_out)
                .replace(R.id.flFragmentContainer, fragment, fragmentNavTag)
                .addToBackStack(fragmentNavTag)
                .commit();
    }

    @Override
    public void pop() {
        if (fragmentManager.isStateSaved()) {
            return;
        }

        getSupportFragmentManager().popBackStackImmediate();
    }

    @Override
    public void onBackPressed() {
        if (fragmentManager.getBackStackEntryCount() > 1) {
            pop();
        } else {
            finish();
        }
    }
}
