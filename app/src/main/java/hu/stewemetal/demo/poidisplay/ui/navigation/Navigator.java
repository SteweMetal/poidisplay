package hu.stewemetal.demo.poidisplay.ui.navigation;

import androidx.fragment.app.Fragment;

/**
 * Navigator is a small interface implemented by the single Activity in the application.
 * <p>
 * Fragments can initiate navigation actions using this interface.
 */
public interface Navigator {

    void add(Fragment fragment);

    void pop();

}
