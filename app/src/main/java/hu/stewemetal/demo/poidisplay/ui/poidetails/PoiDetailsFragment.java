package hu.stewemetal.demo.poidisplay.ui.poidetails;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import hu.stewemetal.demo.poidisplay.R;
import hu.stewemetal.demo.poidisplay.domain.model.Poi;
import hu.stewemetal.demo.poidisplay.ui.NavFragment;

public class PoiDetailsFragment extends NavFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    private static final String KEY_TITLE = "KEY_TITLE";
    private static final String KEY_POIS = "KEY_POIS";


    private Toolbar toolbar;

    private String toolbarTitle;
    private List<Poi> pois = new ArrayList<>();

    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    public PoiDetailsFragment() {
    }

    @SuppressWarnings("deprecation")
    public static PoiDetailsFragment newInstance(@NonNull String title, @NonNull List<Poi> pois) {
        PoiDetailsFragment fragment = new PoiDetailsFragment();

        Bundle args = new Bundle();
        args.putString(KEY_TITLE, title);
        args.putParcelableArrayList(KEY_POIS, new ArrayList<>(pois));
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_poi_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        toolbar = view.findViewById(R.id.toolbar);

        initArguments();

        setupToolbar();
        setupMaps();
    }

    private void initArguments() {
        Bundle arguments = getArguments();

        if (arguments != null) {
            toolbarTitle = arguments.getString(KEY_TITLE);

            List<Poi> argPois = arguments.getParcelableArrayList(KEY_POIS);

            if (argPois != null) {
                pois.addAll(argPois);
            }
        }
    }

    private void setupToolbar() {
        toolbar.setTitle(toolbarTitle);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigator.pop();
            }
        });
    }

    private void setupMaps() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);

        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {

        LatLngBounds.Builder latLngBoundsBuilder = LatLngBounds.builder();

        for (Poi poi : pois) {
            LatLng latLngRepresentation = new LatLng(poi.getLat(), poi.getLon());

            map.addMarker(
                    new MarkerOptions()
                            .position(latLngRepresentation)
                            .title(poi.getName())
            );

            latLngBoundsBuilder.include(latLngRepresentation);
        }

        if (pois.size() > 1) {
            LatLngBounds latLngBounds = latLngBoundsBuilder.build();
            map.moveCamera(
                    CameraUpdateFactory.newLatLngBounds(latLngBounds, 300)
            );
        } else if (pois.size() == 1) {
            Poi singlePoi = pois.get(0);
            map.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                            new LatLng(
                                    singlePoi.getLat(),
                                    singlePoi.getLon()
                            ),
                            16f)
            );
        }

        map.setOnMarkerClickListener(this);
        map.setOnInfoWindowClickListener(this);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        return true;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        marker.hideInfoWindow();
    }
}
