package hu.stewemetal.demo.poidisplay.ui.poilist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import hu.stewemetal.demo.poidisplay.R;
import hu.stewemetal.demo.poidisplay.domain.model.Poi;

public class PoiAdapter extends ListAdapter<Poi, PoiAdapter.PoiViewHolder> {

    interface OnPoiItemClickedListener {
        void onPoiClicked(Poi poi);
    }

    private OnPoiItemClickedListener itemClickListener;

    PoiAdapter(OnPoiItemClickedListener itemClickListener) {
        super(new PoiComparator());

        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public PoiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_poi, parent, false);

        return new PoiViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PoiViewHolder holder, int position) {
        Poi item = getItem(position);

        holder.name.setText(item.getName());
        holder.item = item;
    }

    class PoiViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        Poi item;

        PoiViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.tvPoiItemName);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null && item != null) {
                        itemClickListener.onPoiClicked(item);
                    }
                }
            });
        }
    }
}
