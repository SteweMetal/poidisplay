package hu.stewemetal.demo.poidisplay.ui.poilist;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import hu.stewemetal.demo.poidisplay.domain.model.Poi;

public class PoiComparator extends DiffUtil.ItemCallback<Poi> {
    @Override
    public boolean areItemsTheSame(@NonNull Poi oldItem, @NonNull Poi newItem) {
        return oldItem.equals(newItem);
    }

    @Override
    public boolean areContentsTheSame(@NonNull Poi oldItem, @NonNull Poi newItem) {
        return oldItem.equals(newItem);
    }
}
