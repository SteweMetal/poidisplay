package hu.stewemetal.demo.poidisplay.ui.poilist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;
import hu.stewemetal.demo.poidisplay.R;
import hu.stewemetal.demo.poidisplay.domain.model.Poi;
import hu.stewemetal.demo.poidisplay.ui.NavFragment;
import hu.stewemetal.demo.poidisplay.ui.poidetails.PoiDetailsFragment;

public class PoiListFragment extends NavFragment implements PoiListScreen, PoiAdapter.OnPoiItemClickedListener {

    private Toolbar toolbar;
    private RecyclerView poiList;
    private FloatingActionButton showAllPois;

    private Snackbar loadingSnackbar;

    private PoiListPresenter presenter;
    private PoiAdapter adapter;

    @Override
    public void onStart() {
        super.onStart();
        presenter = PoiListPresenter.getInstance();
        presenter.attachScreen(this);
        presenter.retrievePois();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_poi_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        toolbar = view.findViewById(R.id.toolbar);
        poiList = view.findViewById(R.id.rvPoiList);
        showAllPois = view.findViewById(R.id.fabShowAllPois);

        loadingSnackbar = Snackbar.make(toolbar, getString(R.string.loading), Snackbar.LENGTH_INDEFINITE);

        setupToolbar();
        setupRecyclerView();
        setupButton();
    }

    private void setupToolbar() {
        toolbar.inflateMenu(R.menu.menu_poi_list);
        toolbar.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.menuRefresh:
                    presenter.refreshPois();
                    return true;
            }
            return false;
        });
    }

    private void setupRecyclerView() {
        adapter = new PoiAdapter(this);
        poiList.setAdapter(adapter);
        poiList.addItemDecoration(new DividerItemDecoration(requireContext(), LinearLayout.VERTICAL));
    }

    private void setupButton() {
        showAllPois.setOnClickListener(v -> showDetailsFragment(presenter.getPois()));
    }

    @Override
    public void onStop() {
        presenter.detachScreen();
        super.onStop();
    }

    @Override
    public void showPoiList(List<Poi> pois) {
        loadingSnackbar.dismiss();
        adapter.submitList(pois);
    }

    @Override
    public void showNetworkError() {
        loadingSnackbar.dismiss();

        final Snackbar snackbar = Snackbar
                .make(poiList, getString(R.string.network_error), Snackbar.LENGTH_INDEFINITE);

        snackbar.setAction(getString(R.string.retry), v -> {
            presenter.retrievePois();
            snackbar.dismiss();
        });

        snackbar.show();
    }

    @Override
    public void showLoading() {
        loadingSnackbar.show();
    }

    @Override
    public void onPoiClicked(Poi poi) {
        showDetailsFragment(poi);
    }

    private void showDetailsFragment(List<Poi> pois) {
        navigator.add(PoiDetailsFragment.newInstance(getString(R.string.all_pois), pois));
    }

    private void showDetailsFragment(Poi poi) {
        List<Poi> pois = new ArrayList<>(1);
        pois.add(poi);
        navigator.add(PoiDetailsFragment.newInstance(poi.getName(), pois));
    }
}

