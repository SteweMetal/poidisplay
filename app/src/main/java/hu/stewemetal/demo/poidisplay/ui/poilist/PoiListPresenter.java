package hu.stewemetal.demo.poidisplay.ui.poilist;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import hu.stewemetal.demo.poidisplay.BuildConfig;
import hu.stewemetal.demo.poidisplay.domain.model.Poi;
import hu.stewemetal.demo.poidisplay.network.NetworkDataSource;
import hu.stewemetal.demo.poidisplay.network.PoiDisplayApi;
import hu.stewemetal.demo.poidisplay.network.exception.NetworkCallException;
import hu.stewemetal.demo.poidisplay.ui.Presenter;

class PoiListPresenter extends Presenter<PoiListScreen> {

    private static final String TAG = PoiListPresenter.class.getSimpleName();

    // Dependencies of this presenter could be properly injected using a DI framework like Dagger.
    //region Dependencies
    private NetworkDataSource networkDataSource;
    //endregion

    private static PoiListPresenter instance;

    static PoiListPresenter getInstance() {
        if (instance == null) {
            instance = new PoiListPresenter(new NetworkDataSource(new PoiDisplayApi()));
        }

        return instance;
    }

    private List<Poi> storedPois = new ArrayList<>();

    private int refreshCooldownCounter = 0;

    private PoiListPresenter(NetworkDataSource networkDataSource) {
        this.networkDataSource = networkDataSource;
    }

    /**
     * Tries to retrieve POI information from the network asynchronously or from the cache.
     */
    void retrievePois() {
        screen.showPoiList(storedPois);

        if (refreshCooldownCounter > 0) {
            refreshCooldownCounter--;
        } else {
            refreshPois();
        }
    }

    /**
     * Tries to retrieve POI information from the network asynchronously.
     */
    void refreshPois() {
        if (screen != null) {
            screen.showLoading();
        }
        new GetPoisAsyncTask(networkDataSource).execute();
    }

    // This method and the storedPois list mimics the functionality of a database (DiskDataSource).
    // As the offline behavior of the application was not specified and the basic SQLite APIs on Android (without
    // any 3rd party libraries) are cumbersome, I have omitted the implementation of a disk data layer.
    List<Poi> getPois() {
        return storedPois;
    }

    class GetPoisAsyncTask extends AsyncTask<Void, Integer, List<Poi>> {

        NetworkDataSource networkDataSource;

        GetPoisAsyncTask(NetworkDataSource networkDataSource) {
            this.networkDataSource = networkDataSource;
        }

        @Override
        protected List<Poi> doInBackground(Void... voids) {
            try {
                return networkDataSource.getPois();
            } catch (IOException | NetworkCallException e) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, e.getMessage());
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<Poi> pois) {
            if (screen != null) {
                if (pois == null) {
                    screen.showPoiList(storedPois);
                    screen.showNetworkError();
                } else {
                    refreshCooldownCounter = 10;
                    storedPois = pois;
                    screen.showPoiList(pois);
                }
            }
        }
    }
}
