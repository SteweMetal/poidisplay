package hu.stewemetal.demo.poidisplay.ui.poilist;

import java.util.List;

import hu.stewemetal.demo.poidisplay.domain.model.Poi;
import hu.stewemetal.demo.poidisplay.ui.Screen;

public interface PoiListScreen extends Screen {
    void showPoiList(List<Poi> pois);

    void showNetworkError();

    void showLoading();
}
